package organizer;

import gui.SimulationGUI;
import jadex.commons.future.IIntermediateFuture;
import jadex.commons.future.IntermediateDefaultResultListener;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.Description;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import locator.Location;
import locator.LocationService;
import provider.CinemaService;
import provider.EventService;
import provider.MuseumService;
import provider.cinema.Movie;
import provider.museum.Museum;
import provider.piletilevi.Event;
import weather.WeatherService;

@Description("Organizer")
@Agent
@RequiredServices({
	@RequiredService(name = "weatherServices", type = WeatherService.class, multiple = true, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
	@RequiredService(name = "locatorServices", type = LocationService.class, multiple = true, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
	@RequiredService(name = "cinemaServices", type = CinemaService.class, multiple = true, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
	@RequiredService(name = "eventServices", type = EventService.class, multiple = true, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
	@RequiredService(name = "museumServices", type = MuseumService.class, multiple = true, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class OrganizerAgent {

    private Location location = new Location(59.42, 24.8);

    @AgentService
    private IIntermediateFuture<WeatherService> weatherServices;
    @AgentService
    private IIntermediateFuture<LocationService> locatorServices;
    @AgentService
    private IIntermediateFuture<CinemaService> cinemaServices;
    @AgentService
    private IIntermediateFuture<EventService> eventServices;
    @AgentService
    private IIntermediateFuture<MuseumService> museumServices;

    private static int SUGGESTED_ITEMS = 5;

    private static SimulationGUI gui;

    @AgentBody
    public void executeBody() throws InterruptedException {
	gui = new SimulationGUI();
	gui.addButtonRow("TEST 1", "Suggest today's action movies in Tallinn", new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		gui.outputField.setText("");
		Calendar now = Calendar.getInstance();
		String year = new Integer(now.get(Calendar.YEAR)).toString();
		String month = new Integer(now.get(Calendar.MONTH)).toString();
		String day = new Integer(now.get(Calendar.DAY_OF_MONTH)).toString();
		getMovies("tallinn", year, month, day, "Action");
	    }
	});
	gui.addButtonRow("TEST 2", "Suggest music events", new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		gui.outputField.setText("");
		getEventsbyCategory("muusika", "");
	    }
	});
	gui.addButtonRow("TEST 3", "Suggest open air museums in Tallinn", new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		gui.outputField.setText("");
		getMuseumsByTypeAndState("Tallinn", "Vaba�humuuseumid");
	    }
	});
	gui.addButtonRow("TEST 4", "Get wheather in selected location", new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		gui.outputField.setText("");
		getWeatherFromLocation();
	    }
	});
	gui.addButtonRow("TEST 5", "Suggest museums in Tallinn", new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		gui.outputField.setText("");
		getMuseumsByState("Tallinn");
	    }
	});
	gui.setVisible(true);
	// System.out.println("OrganizerAgent");
	// getWeatherFromLocation();
	// getMuseumsByState("Tallinn");
	// getMuseumsByType("Vaba�humuuseumid");

    }

    public static void main(String[] args) {
	gui = new SimulationGUI();
	gui.addButtonRow("TEST 5", "Get museums in Tallinn", null);
	gui.addButtonRow("TEST 5", "Get museums in Tallinn", null);
	gui.addButtonRow("TEST 5", "Get museums in Tallinn", null);
	gui.addButtonRow("TEST 5", "Get museums in Tallinn", null);
	gui.addButtonRow("TEST 5", "Get museums in Tallinn", null);
	gui.setVisible(true);
    }

    private void getMuseumsByTypeAndState(final String state, final String type) {
	museumServices.addResultListener(new IntermediateDefaultResultListener<MuseumService>() {

	    @Override
	    public void intermediateResultAvailable(MuseumService museumService) {
		List<Museum> museums = museumService.getMuseumsByStateAndType(state, type);
		StringBuilder sb = new StringBuilder();
		for (Museum museum : museums.subList(0, Math.min(SUGGESTED_ITEMS, museums.size()))) {
		    String ratingStars = new String(new char[museum.getRating()]).replace("\0", " *");
		    sb.append(ratingStars + "  " + museum.shortDescription());
		    sb.append("\n\n");
		}
		gui.outputField.setText(gui.outputField.getText() + sb.toString());
	    }
	});
    }

    private void getMuseumsByType(final String type) {
	museumServices.addResultListener(new IntermediateDefaultResultListener<MuseumService>() {

	    @Override
	    public void intermediateResultAvailable(MuseumService museumService) {
		List<Museum> museums = museumService.getMuseumsByType(type);
		StringBuilder sb = new StringBuilder();
		for (Museum museum : museums) {
		    sb.append(" * " + museum.shortDescription());
		    sb.append("\n");
		}
		gui.outputField.setText(gui.outputField.getText() + sb.toString());
	    }
	});
    }

    private void getMuseumsByState(final String state) {
	museumServices.addResultListener(new IntermediateDefaultResultListener<MuseumService>() {

	    @Override
	    public void intermediateResultAvailable(MuseumService museumService) {
		List<Museum> museums = museumService.getMuseumsByState(state);
		StringBuilder sb = new StringBuilder();
		Collections.sort(museums);
		for (Museum museum : museums.subList(0, Math.min(SUGGESTED_ITEMS, museums.size()))) {
		    String ratingStars = new String(new char[museum.getRating()]).replace("\0", " *");
		    sb.append(ratingStars + "  " + museum.shortDescription());
		    sb.append("\n\n");
		}
		gui.outputField.setText(gui.outputField.getText() + sb.toString());
	    }
	});
    }

    private void getEventsbyCategory(final String type, final String category) {
	eventServices.addResultListener(new IntermediateDefaultResultListener<EventService>() {

	    @Override
	    public void intermediateResultAvailable(EventService eventservice) {
		List<Event> events = eventservice.getEventsByTypeAndCategory(type, category);
		StringBuilder sb = new StringBuilder();
		Map<String, Event> moviesMap = new HashMap<String, Event>();
		for (Event i : events) {
		    moviesMap.put(i.getName(), i);
		}
		List<Event> selectedMovies = new ArrayList<Event>(moviesMap.values());
		Collections.sort(selectedMovies);
		for (Event event : selectedMovies.subList(0, Math.min(SUGGESTED_ITEMS, events.size()))) {
		    String ratingStars = new String(new char[event.getRating()]).replace("\0", " *");
		    sb.append(ratingStars + "  " + event.shortDescription());
		    sb.append("\n\n");
		}
		gui.outputField.setText(gui.outputField.getText() + sb.toString());
	    }
	});

    }

    private void getMovies(final String city, final String year, final String month, final String day, final String genre) {
	cinemaServices.addResultListener(new IntermediateDefaultResultListener<CinemaService>() {
	    public void intermediateResultAvailable(CinemaService cinemaService) {
		List<Movie> movies = cinemaService.getMovieListByDayAndGenre(city, year, month, day, genre);
		StringBuilder sb = new StringBuilder();
		Map<String, Movie> moviesMap = new HashMap<String, Movie>();
		for (Movie i : movies) {
		    moviesMap.put(i.getName(), i);
		}
		List<Movie> selectedMovies = new ArrayList<Movie>(moviesMap.values());
		Collections.sort(selectedMovies);
		for (Movie movie : selectedMovies.subList(0, Math.min(SUGGESTED_ITEMS, selectedMovies.size()))) {
		    String ratingStars = new String(new char[movie.getRating()]).replace("\0", " *");
		    sb.append(ratingStars + "  " + movie.shortDescription());
		    sb.append("\n\n");
		}
		gui.outputField.setText(gui.outputField.getText() + sb.toString());
	    }
	});
    }

    private void getWeatherFromLocation() {
	locatorServices.addResultListener(new IntermediateDefaultResultListener<LocationService>() {
	    public void intermediateResultAvailable(LocationService locationService) {
		location = locationService.getLocation();
		getWeather(location);
	    }
	});
    }

    private void getWeather(final Location location) {
	weatherServices.addResultListener(new IntermediateDefaultResultListener<WeatherService>() {
	    public void intermediateResultAvailable(WeatherService weatherService) {
		gui.outputField.setText(weatherService.getWeather(location).toString());
	    }
	});
    }
}
