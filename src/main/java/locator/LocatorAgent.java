package locator;

import jadex.bridge.IInternalAccess;
import jadex.bridge.service.annotation.Service;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.Implementation;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;


@Agent
@Service
@ProvidedServices(@ProvidedService(type=LocationService.class, implementation=@Implementation(expression="$pojoagent")))
public class LocatorAgent implements LocationService {

	@Agent
	protected IInternalAccess	agent;
	private static JFrame gui = new JFrame("");
	private static JLabel map = new JLabel("");
	private static double latitude = 59.45304;
	private static double longitude = 24.68804;
	
	static void setLocation(int x, int y){
		latitude = 59.68 - ((59.68 - 57.51) * y / 762);
		longitude = 21.66 + ((28.34 - 21.66) * x / 1217);
		DecimalFormat df = new DecimalFormat("#.##");
		String latitudeStr = df.format(latitude);
		String longitudeStr = df.format(longitude);
		gui.setTitle(latitudeStr + "N " + longitudeStr + "E");
	}

	private void createGUI(){
		map.setIcon(new ImageIcon("src/files/kaart.png"));
		map.addMouseListener(new MapMouseListener());

		gui.setResizable(false);
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gui.getContentPane().add(map, BorderLayout.CENTER);
		gui.setSize(1223, 791);
		gui.setVisible(true);	    
	}

	@AgentBody
	public void executeBody() throws NumberFormatException, IOException {
		System.out.println("locatorAgent");
		createGUI();
	}

	@Override
	public Location getLocation() {
		return new Location(latitude, longitude);
	}

}

class MapMouseListener implements MouseListener{

	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}

	public void mouseReleased(MouseEvent e) {
		LocatorAgent.setLocation(e.getX(), e.getY());
	}

}
