package locator;

public interface LocationService {
	public Location getLocation();
}
