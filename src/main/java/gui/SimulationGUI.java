package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class SimulationGUI extends JFrame {

	JPanel buttonsPanel;
	JPanel outputPanel;
	public JTextArea outputField;

	public SimulationGUI() {
		super();
		init();
	}

	private void init() {
		setTitle("Free time organizer");
		setSize(800, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));
		getContentPane().add(buttonsPanel,BorderLayout.PAGE_START);

		outputPanel = new JPanel();
		outputPanel.setLayout(new BoxLayout(outputPanel, BoxLayout.Y_AXIS));
		outputPanel.add(addLabel("Results"));
		
		outputField = new JTextArea(" ");
		outputField.setEditable(false);
		outputField.setLineWrap(true);
		JScrollPane scrollPane = new JScrollPane(outputField); 
		outputField.setVisible(true);
		outputPanel.add(scrollPane);
		getContentPane().add(outputPanel,BorderLayout.CENTER);

	}

	public void addButtonRow(String btnTitle, String label,
			ActionListener btnListener) {
		JPanel panel = new JPanel();
		JButton button = new JButton(btnTitle);
		panel.add(button);
		panel.add(addLabel(label));
		FlowLayout fl = new FlowLayout(FlowLayout.LEFT);
		panel.setLayout(fl);
		button.addActionListener(btnListener);
		buttonsPanel.add(panel);
	}

	private JLabel addLabel(String label) {
		JLabel l = new JLabel(label);
		l.setVisible(true);
		return l;
	}

}
