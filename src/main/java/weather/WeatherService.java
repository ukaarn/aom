package weather;

import locator.Location;

public interface WeatherService {
	public Weather getWeather(Location location);
}
