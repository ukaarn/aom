package weather;

public class Weather {
	private String name;
	private String phenomenon;
	private double visibility;
	private double temperature;
	private int windDirection;
	private double windSpeed;
	
	@Override
	public String toString() {
		return "Weather [name=" + name + ", phenomenon=" + phenomenon
				+ ", visibility=" + visibility + ", temperature=" + temperature
				+ ", windDirection=" + windDirection + ", windSpeed="
				+ windSpeed + "]";
	}

	public String getName() {
		return name;
	}

	public String getPhenomenon() {
		return phenomenon;
	}

	public double getVisibility() {
		return visibility;
	}

	public double getTemperature() {
		return temperature;
	}

	public int getWindDirection() {
		return windDirection;
	}

	public double getWindSpeed() {
		return windSpeed;
	}

	public Weather(String name, String phenomenon, double visibility,
			double temperature, int windDirection, double windSpeed) {
		super();
		this.name = name;
		this.phenomenon = phenomenon;
		this.visibility = visibility;
		this.temperature = temperature;
		this.windDirection = windDirection;
		this.windSpeed = windSpeed;
	}
}