package weather;

import com.grum.geocalc.Coordinate;
import com.grum.geocalc.DegreeCoordinate;
import com.grum.geocalc.EarthCalc;
import com.grum.geocalc.Point;

public class Wmo {
	private String name = "";
	@Override
	public String toString() {
		return "Wmo [name=" + name + ", coordinates=" + coordinates + "]";
	}
	private Point coordinates;

	public Wmo(double latitude, double longitude) {
		Coordinate lat = new DegreeCoordinate(latitude);
        Coordinate lng = new DegreeCoordinate(longitude);
        coordinates = new Point(lat, lng);
	}

	public Wmo(String name, double latitude, double longitude) {
		this(latitude, longitude);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Double getDistanceFromLocation(Wmo otherLocation) {
		return EarthCalc.getDistance(coordinates, otherLocation.coordinates);
	}
}
