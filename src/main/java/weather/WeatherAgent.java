package weather;

import jadex.bridge.IInternalAccess;
import jadex.bridge.service.annotation.Service;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.Implementation;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import locator.Location;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Agent
@Service
@ProvidedServices(@ProvidedService(type=WeatherService.class, implementation=@Implementation(expression="$pojoagent")))
public class WeatherAgent implements WeatherService{

	@Agent
	protected IInternalAccess	agent;
	
	private static List<Wmo> wmos = new ArrayList<Wmo>();
	private static Map<String, Weather> currentWeather = new HashMap<String, Weather>();

	public static void main(String[] args) throws NumberFormatException, IOException, ParserConfigurationException, SAXException {
		WeatherAgent w = new WeatherAgent();
		w.executeBody();
        System.out.println(w.getWeather(new Location(59.45304, 24.68804)));
	}
	
	@AgentBody
	public void executeBody() throws NumberFormatException, IOException {
		initWmosCoordinates();
		System.out.println("weatherAgent");
	}
	
	@Override
	public Weather getWeather(Location location) {
		try {
			getCurrentWeather();
		} catch (ParserConfigurationException | SAXException | IOException e) { e.printStackTrace(); }
        Weather weather = currentWeather.get(getClosestWmoName(location.getLatitude(), location.getLongitude()));
        return weather;
	}
	
	private void initWmosCoordinates() throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new FileReader("src/files/wmos.txt"));
		String line;
		while ((line = br.readLine()) != null) {
			String[] wmoStrs = line.split(";");
			Wmo wmo = new Wmo(wmoStrs[0], Double.parseDouble(wmoStrs[1]), Double.parseDouble(wmoStrs[2]));
			wmos.add(wmo);
		}
		br.close();
	}
	
	private void getCurrentWeather() throws ParserConfigurationException, SAXException, IOException{
		String url = "http://www.ilmateenistus.ee/ilma_andmed/xml/observations.php";
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        NodeList stations = builder.parse(url).getElementsByTagName("station");
        for (int i = 0; i < stations.getLength(); i++) {
            Element station = (Element) stations.item(i);
            if (station.getNodeType() != Node.ELEMENT_NODE)
                continue;
            getStationData(station);
        }
	}
	
	private void getStationData(Element station) {
		String name = getWeatherProperty(station, "name");
    	String phenomenon = getWeatherProperty(station, "phenomenon");
    	double visibility = Double.parseDouble(getWeatherProperty(station, "visibility"));
    	double temperature = Double.parseDouble(getWeatherProperty(station, "airtemperature"));
    	int windDirection = Integer.parseInt(getWeatherProperty(station, "winddirection"));
    	double windSpeed = Double.parseDouble(getWeatherProperty(station, "windspeed"));
        Weather weather = new Weather(name, phenomenon, visibility, temperature, windDirection, windSpeed);
        currentWeather.put(name, weather);
	}

	private String getWeatherProperty(Element station, String weatherProperty){
		try {
	        Element weatherPropertyElement = (Element) station.getElementsByTagName(weatherProperty).item(0);
	        return weatherPropertyElement.getChildNodes().item(0).getNodeValue();
		} catch (Exception e) {
			return"-999";
		}	
	}
	
	private String getClosestWmoName(double latitude, double longitude) {
		Wmo location = new Wmo(latitude, longitude);
		String wmoName = "";
		Double distance = Double.MAX_VALUE;
		for (Wmo wmo : wmos) 
			if(distance > wmo.getDistanceFromLocation(location)){
				distance = wmo.getDistanceFromLocation(location);
				wmoName = wmo.getName();
			}					
		return wmoName;
	}
}
