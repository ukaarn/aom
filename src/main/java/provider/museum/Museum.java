package provider.museum;

import provider.cinema.Movie;


public class Museum implements Comparable<Museum>{

	private String type, state, name, link, adr, tel, email, url, desc, ticket, photo;
	private String mon, tue, wed, thu, fri, sat, sun;


	public String getMon() {
		return mon;
	}

	public String getTue() {
		return tue;
	}

	public String getWed() {
		return wed;
	}

	public String getThu() {
		return thu;
	}

	public String getFri() {
		return fri;
	}

	public String getSat() {
		return sat;
	}

	public String getSun() {
		return sun;
	}



	@Override
	public String toString() {
		return "Museum [type=" + type + ", state=" + state + ", name=" + name
				+ ", link=" + link + ", adr=" + adr + ", tel=" + tel
				+ ", email=" + email + ", url=" + url + ", desc=" + desc
				+ ", ticket=" + ticket + ", photo=" + photo + ", mon=" + mon
				+ ", tue=" + tue + ", wed=" + wed + ", thu=" + thu + ", fri="
				+ fri + ", sat=" + sat + ", sun=" + sun + "]";
	}
	
	public String shortDescription() {
		return name+", address: " + adr + ", desc: " + desc
				+ ", tickets:" + ticket;
	}


	public Museum(String type, String state, String name, String link,
			String adr, String tel, String email, String url, String desc,
			String ticket, String photo, String mon, String tue, String wed,
			String thu, String fri, String sat, String sun) {
		super();
		this.type = type;
		this.state = state;
		this.name = name;
		this.link = link;
		this.adr = adr;
		this.tel = tel;
		this.email = email;
		this.url = url;
		this.desc = desc;
		this.ticket = ticket;
		this.photo = photo;
		this.mon = mon;
		this.tue = tue;
		this.wed = wed;
		this.thu = thu;
		this.fri = fri;
		this.sat = sat;
		this.sun = sun;
	}

	public String getType() {
		return type;
	}

	public String getState() {
		return state;
	}

	public String getName() {
		return name;
	}

	public String getLink() {
		return link;
	}

	public String getAdr() {
		return adr;
	}

	public String getTel() {
		return tel;
	}

	public String getEmail() {
		return email;
	}

	public String getUrl() {
		return url;
	}

	public String getDesc() {
		return desc;
	}

	public String getTicket() {
		return ticket;
	}

	public String getPhoto() {
		return photo;
	}
	
	public Integer getRating(){
		return 1 + name.length() % 5;
	}
	
	@Override
	public int compareTo(Museum arg0) {
		return arg0.getRating().compareTo(getRating());
	}
	

			/*
	private static PrintWriter out, out2;
	public static void main(String[] args) {
		try {
			out = new PrintWriter("museums.txt");
			out2 = new PrintWriter("museums2.txt");
			Document doc = Jsoup.connect("http://www.muuseum.ee/et/muuseumid/eesti_muuseumid/").get();
			for (Element museum : doc.select(".floatright a")){
				String link = museum.attr("href");
				String name = museum.text();
				getStateMuseums(name, link);
			}
			doc = Jsoup.connect("http://www.muuseum.ee/et/muuseumid/eesti_muuseumid/by_theme/").get();
			for (Element museum : doc.select(".sisu .alphafloat a")){
				String link = museum.attr("href");
				String name = museum.text();
				getCatMuseums(name, link);
			}
				out.close();
				out2.close();
		} catch (IOException e) {e.printStackTrace();}
		

	}

	private static void getCatMuseums(String cat, String url) throws IOException {
		Document doc = Jsoup.connect(url).get();
		Elements museums = doc.select(".sisu a");
		for(int i=4; i< museums.size()-1;i++){
			Element museum = museums.get(i);
			String link = museum.attr("href");
			String name = museum.text();
			getMuseumInfo(out2, cat, name, link);
		}
	}

	private static void getStateMuseums(String state, String url) throws IOException {
		Document doc = Jsoup.connect(url).get();
			for (Element museum : doc.select(".detaill a")){
				String link = museum.attr("href");
				String name = museum.text();
				getMuseumInfo(out, state, name, link);
			}
	}

	private static void getMuseumInfo(PrintWriter out, String state, String name, String url) throws IOException {
		Document doc = Jsoup.connect(url).get();
		String data = state + ";;" + name + ";;" + url;
		Element museum = doc.select(".rightblock").first();
		for (Element info : museum.select(".obj div")) {
			if(!info.text().equals(" "))
				data += ";;" + info.text().substring(1);
		}
		data += ";;" + museum.select(".detaill p").text();
		for (Element picto : museum.select(".detailr .picto")) {
			data += ";;" + picto.attr("src").substring(35, picto.attr("src").length()-4 );
		}
		out.println(data);
	}
*/
}