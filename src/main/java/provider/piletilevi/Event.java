package provider.piletilevi;

import provider.cinema.Movie;

public class Event implements Comparable<Event>{

	private String img, link, time, name, location, price;

	public Event(String img, String link, String time, String name,
			String location, String price) {
		super();
		this.img = img;
		this.link = link;
		this.time = time;
		this.name = name;
		this.location = location;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Event [img=" + img + ", link=" + link + ", time=" + time
				+ ", name=" + name + ", location=" + location + ", price="
				+ price + "]";
	}
	
	public String shortDescription() {
		return name + ", Rating: "+getRating()+", time:" + time+", location:" + location + ", price:"
				+ price;
	}

	public String getImg() {
		return img;
	}

	public String getLink() {
		return link;
	}

	public String getTime() {
		return time;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}

	public String getPrice() {
		return price;
	}
	
	public Integer getRating(){
		return 1 + name.length() % 5;
	}
	
	@Override
	public int compareTo(Event arg0) {
		return arg0.getRating().compareTo(getRating());
	}
	

}
