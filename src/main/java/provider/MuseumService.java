package provider;

import java.util.List;

import provider.museum.Museum;

public interface MuseumService {
	public List<Museum> getMuseumsByState(String state);
	public List<Museum> getMuseumsByStateAndType(String state, String type);
	public List<Museum> getMuseumsByType(String type);
}
