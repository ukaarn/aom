package provider;

import jadex.bridge.IInternalAccess;
import jadex.bridge.service.annotation.Service;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.Implementation;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import provider.museum.Museum;

@Agent
@Service
@ProvidedServices(@ProvidedService(type=MuseumService.class, implementation=@Implementation(expression="$pojoagent")))
public class MuseumAgent implements MuseumService{
	@Agent
	protected IInternalAccess	agent;
	private List<Museum> museums = new ArrayList<Museum>();	
	public MuseumAgent() {
		super();
		System.out.println("MuseumAgent");
		try {
			BufferedReader br = new BufferedReader(new FileReader("src/files/museum.csv"));
			String line;
			while ((line = br.readLine()) != null) {
				String[] museumStrs = line.split(";");
				String type = museumStrs[0];
				String state = museumStrs[1];
				String name = museumStrs[2];
				String link = museumStrs[3];
				String adr = museumStrs[4];
				String tel = museumStrs[5];
				String email = museumStrs[6];
				String url = museumStrs[7];
				String mon = museumStrs[8];
				String tue = museumStrs[9];
				String wed = museumStrs[10];
				String thu = museumStrs[11];
				String fri = museumStrs[12];
				String sat = museumStrs[13];
				String sun = museumStrs[14];
				String desc = museumStrs[15];
				String ticket = museumStrs[16];
				String photo = museumStrs[17];
				
				Museum museum = new Museum(type, state, name, link, adr, tel, email, url, desc, ticket, photo, mon, tue, wed, thu, fri, sat, sun);
				museums.add(museum);
			}
			br.close();
		} catch (IOException e) {e.printStackTrace();}
		
	}

	public static void main(String[] args) throws IOException {
		MuseumAgent agent = new MuseumAgent();
		for (Museum museum : agent.museums) {
			System.out.println(museum);
		}
		System.out.println("========");
		List<Museum> museums = agent.getMuseumsByState("Tallinn");
		for (Museum museum : museums) {
			System.out.println(museum);
		}
		System.out.println("========");
		museums = agent.getMuseumsByType("Paigamuuseumid");
		for (Museum museum : museums) {
			System.out.println(museum);
		}
		System.out.println("========");
		museums = agent.getMuseumsByStateAndType("Tallinn","Teadus- ja tehnikamuuseumid");
		for (Museum museum : museums) {
			System.out.println(museum);
		}
		

	}
	
	public List<Museum> getMuseumsByState(String state){
		List<Museum> filteredMuseums = new ArrayList<Museum>();
		for (Museum museum : museums){
			if(museum.getState().equals(state))
				filteredMuseums.add(museum);
		}
		return filteredMuseums;
	}	
	
	public List<Museum> getMuseumsByStateAndType(String state, String type){
		List<Museum> filteredMuseums = new ArrayList<Museum>();
		for (Museum museum : museums) 
			if(museum.getState().equals(state) && museum.getType().equals(type))
				filteredMuseums.add(museum);
		return filteredMuseums;
	}	
	
	public List<Museum> getMuseumsByType(String type){
		List<Museum> filteredMuseums = new ArrayList<Museum>();
		for (Museum museum : museums) 
			if(museum.getType().equals(type))
				filteredMuseums.add(museum);
		return filteredMuseums;
	}
}
