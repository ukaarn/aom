package provider;

import jadex.bridge.IInternalAccess;
import jadex.bridge.service.annotation.Service;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.Implementation;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import provider.cinema.Cinema;
import provider.cinema.Human;
import provider.cinema.Movie;
import provider.cinema.Session;
@Agent
@Service
@ProvidedServices(@ProvidedService(type=CinemaService.class, implementation=@Implementation(expression="$pojoagent")))
public class SolarisAgent implements Cinema{
	public SolarisAgent() {
		super();
		System.out.println("SolarisAgent");
	}

	@Agent
	protected IInternalAccess	agent;
	private List<Movie> movieList;
	private String genreCode = "0";
	
	public static void main(String[] args) throws Exception {
		Cinema cinema = new SolarisAgent();
		List<Movie> movieList = cinema.getMovieListByDayAndGenre("2015", "03", "23", "");
		for (Movie movie : movieList) 
			System.out.println(movie);
	}

	private void getMovieData(Element movieEl) {
		List<Human> producers = null, actors = null;
		String link = "http://solariskino.ee/" + movieEl.select("th a").attr("href");
		String image = movieEl.select("th a img").attr("src");
		String ageRestriction = movieEl.select(".studio-rating img").attr("alt");
		String name = movieEl.select("th a").attr("title");
		String originalName = movieEl.select(".title-original").text();
		String description = "";
		String length = "";
		String genre = movieEl.select(".genre").text();
		List<Session> sessionsList = new ArrayList<Session>();
		String location = movieEl.select(".theatre-name").text();
		String time = movieEl.select(".hours").text();
		String placeInfo = movieEl.select(".seats-count strong").text();
		sessionsList.add(new Session(location + " " + time, placeInfo ));
		Movie movie = new Movie(link, image, ageRestriction, name, originalName, length, description, genre, producers, actors, sessionsList);
		movieList.add(movie);
	}

	@Override
	public void setCinema(String cinema) {}

	@Override
	public void getGenreCode(String genre) {
		switch (genre) {
		case "Action": genreCode = "15"; break;
		case "Animation": genreCode = "30"; break;
		case "Comedy": genreCode = "31"; break;
		case "Drama": genreCode = "35"; break;
		case "Fantasy": genreCode = "19"; break;
		case "Romance": genreCode = "34"; break;
		case "Sci-fi": genreCode = "32"; break;
		case "Sci-fi2": genreCode = "20"; break;
		case "Thriller": genreCode = "23"; break;
		default: genreCode = "0"; break;
		}		
	}

	@Override
	public List<Movie> getMovieListByDayAndGenre(String year, String month,	String day, String genre) {
		movieList = new ArrayList<Movie>();
		String date = year + "/" + month + "/" + day + "/";
		getGenreCode(genre);
		try {
			Document doc = Jsoup.connect("http://solariskino.ee/et/kinokavad/" + 
					date + 
					"?genre=" + genreCode).get();
			for (Element movie : doc.select(".item")) 
				getMovieData(movie);
		} catch (IOException e) {e.printStackTrace();}
		return movieList;
	}

	@Override
	public List<Movie> getMovieListByDayAndGenre(String cinema, String year, String month, String day, String genre) {
		return getMovieListByDayAndGenre(year, month, day, genre);
	}


}
