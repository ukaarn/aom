package provider;

import java.util.List;

import provider.piletilevi.Event;

public interface EventService {
	public List<Event> getEventsByTypeAndCategory(String type, String category);
}
