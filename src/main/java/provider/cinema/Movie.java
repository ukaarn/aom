package provider.cinema;

import java.util.List;

public class Movie implements Comparable<Movie>{

	private String link, image, ageRestriction, name, originalName, length, description, genre;
	private List<Human> producers, actors; 
	private List<Session> sessionsList;
	
	public String getLink() {
		return link;
	}

	public String getImage() {
		return image;
	}

	public String getAgeRestriction() {
		return ageRestriction;
	}

	public String getName() {
		return name;
	}

	public String getOriginalName() {
		return originalName;
	}

	public String getLength() {
		return length;
	}

	public String getDescription() {
		return description;
	}

	public String getGenre() {
		return genre;
	}

	public List<Human> getProducers() {
		return producers;
	}

	public List<Human> getActors() {
		return actors;
	}

	public List<Session> getSessionsList() {
		return sessionsList;
	}

	public Movie(String link, String image, String ageRestriction, String name,
			String originalName, String length, String description,
			String genre, List<Human> producers, List<Human> actors,
			List<Session> sessionsList) {
		super();
		this.link = link;
		this.image = image;
		this.ageRestriction = ageRestriction;
		this.name = name;
		this.originalName = originalName;
		this.length = length;
		this.description = description;
		this.genre = genre;
		this.producers = producers;
		this.actors = actors;
		this.sessionsList = sessionsList;
	}

	@Override
	public String toString() {
		return "Movie [link=" + link + ", image=" + image + ", ageRestriction="
				+ ageRestriction + ", name=" + name + ", originalName="
				+ originalName + ", length=" + length + ", description="
				+ description + ", genre=" + genre + ", producers=" + producers
				+ ", actors=" + actors + ", sessionsList=" + sessionsList + "]";
	}
	
	public String shortDescription(){
		String result =  name + " \t(" + originalName + ") \t"+ length+" \t"+genre+" ";
		if(sessionsList != null && sessionsList.size() > 0){
			result += "\t"+sessionsList.get(0);
		}
		return result;
	}
	
	public Integer getRating(){
		return 1 + name.length() % 5;
	}

	@Override
	public int compareTo(Movie arg0) {
		return arg0.getRating().compareTo(getRating());
	}
	


}
