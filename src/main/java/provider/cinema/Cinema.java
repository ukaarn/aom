package provider.cinema;

import java.util.List;

import provider.CinemaService;

public interface Cinema extends CinemaService{
	
	void setCinema(String cinema);
	void getGenreCode(String genre);
	
	public List<Movie> getMovieListByDayAndGenre(String year, String month, String day, String genre);
	
}
