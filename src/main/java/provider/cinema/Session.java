package provider.cinema;

public class Session {
	@Override
	public String toString() {
		return "Session " + locationAndTime;
	}
	public Session(String locationAndTime, String info) {
		super();
		this.locationAndTime = locationAndTime;
		this.info = info;
	}
	public String getLocationAndTime() {
		return locationAndTime;
	}
	public String getInfo() {
		return info;
	}
	private String locationAndTime;
	private String info;
}
