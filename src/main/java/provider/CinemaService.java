package provider;

import java.util.List;

import provider.cinema.Movie;

public interface CinemaService {

	public List<Movie> getMovieListByDayAndGenre(String cinema, String year, String month, String day, String genre);
}
