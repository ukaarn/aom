package provider;

import jadex.bridge.IInternalAccess;
import jadex.bridge.service.annotation.Service;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.Implementation;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import provider.piletilevi.Event;

@Agent
@Service
@ProvidedServices(@ProvidedService(type=EventService.class, implementation=@Implementation(expression="$pojoagent")))
public class PiletileviAgent implements EventService {
	public PiletileviAgent() {
		super();
		System.out.println("PiletileviAgent");
	}

	@Agent
	protected IInternalAccess	agent;
	private List<Event> eventList = new ArrayList<Event>();
	
	public static void main(String[] args) {
		PiletileviAgent piletilevi = new PiletileviAgent();
		piletilevi.getEventsByTypeAndCategory("sport", "");
		for (Event event : piletilevi.eventList) {
			System.out.println(event);
		}
	}

	public List<Event> getEventsByTypeAndCategory(String type, String category){
		String url = "http://www.piletilevi.ee/est/piletid/" + type + "/";
		if(!category.equals(""))
			url += category + "/";
		try {
			Document doc = Jsoup.connect(url).get();
			for (Element event : doc.select(".event_short")) 
				eventList.add(getEventData(event));
		} catch (IOException e) {e.printStackTrace();}
		return eventList;
	}
	
	private Event getEventData(Element eventEl) {
		String img = eventEl.select(".event_short_image").attr("src");
		String link = eventEl.select(".event_short_inner").attr("href");
		String time = eventEl.select(".event_short_date").text();
		String name = eventEl.select(".event_short_title").text();
		String location = eventEl.select(".event_short_venue_text").text();
		String price = eventEl.select(".event_short_price_text").text();
		Event event = new Event(img, link, time, name, location, price);
		return event;
	}

}
