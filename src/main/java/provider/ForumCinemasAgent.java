package provider;

import jadex.bridge.IInternalAccess;
import jadex.bridge.service.annotation.Service;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.Implementation;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import provider.cinema.Cinema;
import provider.cinema.Human;
import provider.cinema.Movie;
import provider.cinema.Session;
@Agent
@Service
@ProvidedServices(@ProvidedService(type=CinemaService.class, implementation=@Implementation(expression="$pojoagent")))
public class ForumCinemasAgent implements Cinema{

	
	@Agent
	protected IInternalAccess	agent;
	private int cinemaCode = 1008;
	private List<Movie> movieList;
	private String genreCode = "";
	
	public static void main(String[] args) throws Exception {
		Cinema cinema = new ForumCinemasAgent();
		List<Movie> movieList = cinema.getMovieListByDayAndGenre("2015", "04", "08", "");
		for (Movie movie : movieList) 
			System.out.println(movie);
	}

	private void getMovieData(Element movieEl) {
		List<Human> producers = null, actors = null;
		String link = movieEl.select(".eventImageDiv a").attr("href");
		String image = movieEl.select(".eventImageDiv a img").attr("src");
		String ageRestriction = movieEl.select(".agelimitBadge").attr("title");
		Element movieInfo = movieEl.select(".small_txt").first();
		String name = movieInfo.select(".result_h").html();
		String originalName = movieInfo.select("div").first().html();
		String description = movieInfo.select("div").get(1).html();
		String length = movieInfo.select("div").get(3).select("b").get(1).html();
		for (int i=1; i< movieInfo.getAllElements().size(); movieInfo.getAllElements().get(i++).html(""));
		String genre = movieInfo.text();
		if(movieEl.select(".halfbr").size()>0)
			producers = getNames(movieEl.select(".halfbr").first());
		if(movieEl.select(".halfbr").size()>1)
			actors = getNames(movieEl.select(".halfbr").get(1));
		Elements sessions = movieEl.select(".tableGradient tbody tr td");
		List<Session> sessionsList = new ArrayList<Session>();
		for (Element session : sessions)
			sessionsList.add(new Session(session.select(".tooltipLabel").text(), session.select("img").attr("title")));
		Movie movie = new Movie(link, image, ageRestriction, name, originalName, length, description, genre, producers, actors, sessionsList);
		movieList.add(movie);
	}

	private static List<Human> getNames(Element names) {
		List<Human> people = new ArrayList<Human>();
		for (Element name : names.select("a")) {
			people.add(new Human(name.text()));
		}
		return people;
	}


	@Override
	public List<Movie> getMovieListByDayAndGenre(String year, String month, String day, String genre) {
		movieList = new ArrayList<Movie>();
		String date = day + "." + month + "." + year;
		getGenreCode(genre);
		try {
			String url = "http://www.forumcinemas.ee/Movies/NowInTheatres?" + 
					"dt=" + date + 
					"&genre=" + genreCode + 
					"&X-Requested-With=XMLHttpRequest"+
					"&area=" + cinemaCode;
			System.out.println(url);
			Document doc = Jsoup.connect(url).get();
			for (Element movie : doc.select(".result")) 
				getMovieData(movie);
		} catch (IOException e) {e.printStackTrace();}
		return movieList;
	}
	
	

	public void getGenreCode(String genre) {
		switch (genre) {
			case "Action": genreCode = "1043"; break;
			case "Adventure": genreCode = "1049"; break;
			case "Animation": genreCode = "1053"; break;
			case "Ballet": genreCode = "1516"; break;
			case "Biography": genreCode = "1383"; break;
			case "Comedy": genreCode = "1041"; break;
			case "Crime": genreCode = "1382"; break;
			case "Drama": genreCode = "1042"; break;
			case "Fantasy": genreCode = "1058"; break;
			case "Family": genreCode = "1068"; break;
			case "Oper": genreCode = "1074"; break;
			case "Romance": genreCode = "1052"; break;
			case "Sci-fi": genreCode = "1044"; break;
			case "Thriller": genreCode = "1047"; break;
			default: genreCode = ""; break;
		}		
	}

	@Override
	public void setCinema(String cinema) {
		switch (cinema) {
			case "tallinn": cinemaCode = 1008; break;
			case "tartu": cinemaCode = 1005; break;
			case "narva": cinemaCode = 1004; break;
			default: cinemaCode = 1008; break;
		}
		
	}

	@Override
	public List<Movie> getMovieListByDayAndGenre(String cinema, String year, String month, String day, String genre) {
		setCinema(cinema);
		return getMovieListByDayAndGenre(year, month, day, genre);
	}

}
